import {observable, action} from 'mobx';

const todoItems = observable({
	todos: [],
	dones: [],
	addTodo: action(function addTodo(todo){
		todoItems.todos.push(todo);
	}),
	finishTodo: action(function finishTodo(index, todo){
		todoItems.todos.splice(index,1);
		todoItems.dones.push(todo);
	}),
	getList: action(function getList(){
		return todoItems.todos;
	}),
	getDone: action(function getDone(){
		return todoItems.dones;
	})
});

export default todoItems;;
