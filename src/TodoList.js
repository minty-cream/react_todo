import React, { Component } from 'react';
import {observer} from 'mobx-react';
import TodoListItem from './TodoListItem';
import AddTodoListItem from './AddTodoListItem';
import TodoListStore from './TodoListStore';
import './App.css';

class TodoList extends Component {
	constructor(props) {
		super(props);
		this.createList = this.createList.bind(this);
		this.createFinished = this.createFinished.bind(this);
	}

	render() {
		return (
			<div>
				<ol>
					{this.createList()}
				</ol>
				<h2>Finished</h2>
				<ul>
					{this.createFinished()}
				</ul>
				<AddTodoListItem/>
			</div>
		);
	}

	createList() {
		if(TodoListStore.getList().length > 0){
			return TodoListStore.getList().map((todo, key)=>{
				return(<li key={key}><TodoListItem index={key} value={todo}/></li>);
			});
		}else{
			return(<li><span>Add some todo items!</span></li>);
		}
	}

	createFinished() {
		if(TodoListStore.getDone().length > 0){
			return TodoListStore.getDone().map((todo, key)=>{
				return(<li key={key}>{todo}</li>);
			});
		}else{
			return null;
		}
	}
}

export default observer(TodoList);
