import React, { Component } from 'react';
import TodoListStore from './TodoListStore';
import './App.css';

class AddTodoListItem extends Component {

	constructor(props) {
		super(props);
		this.state = {value: ''};

		this.handleChange = this.handleChange.bind(this);
		this.addTodoItem = this.addTodoItem.bind(this);
	}

	render() {
		return (
			<div>
				<form onSubmit={this.addTodoItem}>
					<input type="text" value={this.state.value} onChange={this.handleChange}/>
					<input type="submit" value="Add Todo Item"/>
				</form>
			</div>
		);
	}

	handleChange(event){
		this.setState({value: event.target.value});
	}

	addTodoItem(event){
		TodoListStore.addTodo(this.state.value);
		this.setState({value:''});
		event.preventDefault();
	}
}

export default AddTodoListItem;
