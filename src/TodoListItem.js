import React, { Component } from 'react';
import TodoListStore from './TodoListStore';
import './App.css';

class TodoListItem extends Component {
	constructor(props) {
		super(props);

		this._finishTodo = this._finishTodo.bind(this);
	}

	render() {
		return (
			<div>
				<span>{this.props.value}</span>
				<button onClick={this._finishTodo}>Done!</button>
			</div>
		);
	}

	_finishTodo() {
		TodoListStore.finishTodo(this.props.index, this.props.value);
	}
}

export default TodoListItem;
